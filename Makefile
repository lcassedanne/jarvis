clean:
	@rm -f */version.txt
	@rm -fr *pyc
	@rm -fr ./*/*/*/__pycache__
	@rm -fr ./*/*/__pycache__
	@rm -fr ./*/__pycache__
	@rm -fr ./__pycache__
	@rm -fr jarvis.egg-info
	@rm -fr .eggs/

dev_install:
	@pip3 install -e .

tree:
	@tree -I 'jv_env'


#
# QA
#
qa_lines_count:
	@find . -name '*.py' -exec  wc -l {} \; | sort -n| awk \
	'{printf "%4s %s\n", $$1, $$2}{s+=$$0}END{print s}'
	@echo ''

qa_check_code:
	@flake8 --exclude=*.sh scripts/* jarvis/*.py tests/*.py jarvis/*/*.py jarvis/*/*/*.py
