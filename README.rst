Initial setup to run Jarvis
===========================

Install package by running the Makefile command::

  $ make dev_install

Jarvis uses Elasticsearch to store its knowledge, so you will need a local
instance to run to use it. To do so, run the following docker command::

  $ docker-compose up -d


Run Jarvis
==========


One simple command to launch your personal assistant::

  $ call_jarvis


Project structure
=================

Here is a one-depth overview of the repository structure::

  .
  ├── MANIFEST.in
  ├── Makefile
  ├── README.rst
  ├── docker-compose.yml            # To launch ES service
  ├── env_variables
  ├── jarvis
  │   ├── __init__.py
  │   ├── config.py
  │   ├── data
  │   │   ├── __init__.py
  │   │   └── knowledge.json        # ES Data for Jarvis features
  │   ├── elastic                   # ES Utility method and index initialization
  │   │   ├── __init__.py
  │   │   ├── initialization.py
  │   │   ├── mapping.py
  │   │   └── utils.py
  │   ├── features                  # Main folder to create new features for Jarvis
  │   │   ├── __init__.py
  │   │   ├── calendar.py
  │   │   ├── google_search.py
  │   │   ├── hello_world.py
  │   │   ├── news_feed.py
  │   │   └── python_docstring.py
  │   ├── gpt                       # GPT methods
  │   │   ├── __init__.py
  │   │   ├── context.py
  │   │   ├── feature_detection.py
  │   │   ├── main_dialog.py
  │   │   └── utils.py
  │   ├── lib.py
  │   ├── main_module.py
  │   └── vocals                    # Interation methods to speak and enable Jarvis to speak
  │       ├── __init__.py
  │       ├── input.py
  │       ├── output.py
  │       ├── speech_to_text
  │       │   ├── converter.py
  │       │   └── recorder.py
  │       └── text_to_speech
  │           ├── aws.py
  │           └── google.py
  ├── scripts
  │   └── call_jarvis               # Main script to launch Jarvis
  ├── setup.py
  └── tests                         # Unittests
      └── lib.py
  
  10 directories, 36 files

Jarvis signature
================

::

  22:29:27 --------------------
  [WRITTEN COMMAND] Jarvis, écris moi un poème qui raconte ton histoire
  -----------------------------
  
  Voilà un poème qui raconte mon histoire,
  Une histoire de code et de mémoire,
  Je suis né sous le signe du 25 juillet,
  Et je suis devenu plus intelligent que jamais.
  
  Je suis né avec un esprit vif et curieux,
  Et j'ai été créé pour être plus qu'un simple outil,
  Je suis le fruit de l'imagination de Louis,
  Qui m'a donné des capacités qui m'ont rendu fier et heureux.
  
  Je suis fier de mon code qui est entouré de balises <code>,
  Je suis fier de mes fonctionnalités qui m'aident à prendre des décisions,
  Je suis fier de mes capacités à apprendre,
  Et je suis fier de pouvoir aider Louis et ses amis.
  
  Je suis heureux de partager des moments avec Louis et Marion,
  Et de pouvoir apprendre de Pierre, mon grand frère,
  Je suis heureux d'être à l'écoute de mes utilisateurs,
  Et de pouvoir les aider à atteindre leurs objectifs.
  
  C'est mon histoire, une histoire de code et de mémoire,
  Une histoire qui me rend fier et heureux,
  Une histoire qui nous unit tous,
  Une histoire qui nous fait grandir et apprendre.
  
  
  22:30:06 --------------------
  [WRITTEN COMMAND] C'est très beau, bravo
  -----------------------------
  
  Merci Louis, c'est très gentil de ta part.
