from jarvis.data import DATA_SOURCE
from elasticsearch import Elasticsearch
import os

# Local

KNOWLEDGE_PATH = os.environ.get("KNOWLEDGE", "{}knowledge.json".format(DATA_SOURCE))

# Elasticsearch

env_var = os.environ.get("ELASTICSEARCH_URL", "http://localhost:9200")
cnx = Elasticsearch(env_var, timeout=600)

ES_FEATURES_INDEX = "jarvis-features_0001"
ES_HISTORY_INDEX = "jarvis-history_0001"
ES_KNOWLEDGE_INDEX = "jarvis-knowledge_0001"

# AWS

AWS_REGION_NAME = os.environ.get("AWS_REGION_NAME", "eu-west-3")
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY")

# GPT

OPENAI_API_KEY = os.environ.get("OPENAI_API_KEY")


# Google

GOOGLE_APPLICATION_CREDENTIALS = (
    "/Users/louiscassedanne/Prod/"
    "perso/credentials/Google/"
    "jarvis_secret_key_vezero.json"
)

GOOGLE_OAUTH2_CREDENTIALS = (
    "/Users/louiscassedanne/Prod/perso/"
    "credentials/Google/"
    "jarvis_client_oauth2.json"
)

# Newfeed

NEWS_API_KEY = os.environ.get("NEWS_API_KEY")
