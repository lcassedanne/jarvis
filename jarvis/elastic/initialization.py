#!/usr/bin/env python
# -*- coding: UTF-8 -*-

"""
"""
import json

from jarvis.config import (
    cnx,
    KNOWLEDGE_PATH,
    ES_FEATURES_INDEX,
    ES_HISTORY_INDEX,
    ES_KNOWLEDGE_INDEX
)

from jarvis.elastic.utils import (
    create_index,
    inject_data
)
from jarvis.elastic.mapping import (
    get_features_mapping,
    get_knowledge_mapping,
    get_history_mapping
)


def check_jarvis_indices():
    """Initialise un index avec un mapping et injecte les données
    du KNOWLEDGE file contenant les features de Jarvis.

    :param index: Nom de l'index Elasticsearch.
    :type index: str
    """
    # Overwrite features_index
    print('* FEATURES: ', end="")
    features_mapping = get_features_mapping()
    create_index(cnx, features_mapping, ES_FEATURES_INDEX, force=True)
    # Inject knowledge in jarvis index
    with open(KNOWLEDGE_PATH, "r") as f:
        data = json.load(f)
    # Only keep active feature
    active_features = [el for el in data
                       if el.get('feature_active')]
    inject_data(cnx, active_features, ES_FEATURES_INDEX)
    print(f"Initialized ([{len(active_features)}] features loaded)")

    # Check history index
    print('* HISTORY: ', end="")
    if cnx.indices.exists(index=ES_HISTORY_INDEX):
        print(f'Found existing "{ES_HISTORY_INDEX}" index.')
    else:
        print(f'[WARNING] Could not find existing "{ES_HISTORY_INDEX}" index. '
              'Creating it')
        history_mapping = get_history_mapping()
        create_index(cnx, history_mapping, ES_HISTORY_INDEX, force=True)

    # Check knowledge index
    print('* KNOWLEDGE: ', end="")
    if cnx.indices.exists(index=ES_KNOWLEDGE_INDEX):
        print(f'Found existing "{ES_KNOWLEDGE_INDEX}" index.')
    else:
        print(f'[WARNING] Could not find existing "{ES_KNOWLEDGE_INDEX}" index. '
              'Creating it')
        knowledge_mapping = get_knowledge_mapping()
        create_index(cnx, knowledge_mapping, ES_KNOWLEDGE_INDEX, force=True)
