

def get_knowledge_mapping():
    """Defines mapping for Jarvis knowledge index"""
    mapp = {'settings': {
                'number_of_shards': 1,
                'number_of_replicas': 0},
            'mappings': {
                'properties': {  # noqa
                    'info_title': {'type': 'text'},  # ADD HERE ANALYZER
                    'info_value': {'type': 'text'},  # ADD HERE ANALYZER
                    'stored_at': {'type': 'date'}}}}
    return mapp


def get_features_mapping():
    """Defines mapping for Jarvis features index"""
    mapp = {'settings': {
                'number_of_shards': 1,
                'number_of_replicas': 0},
            'mappings': {
                'properties': {  # noqa
                    'feature_context': {'type': 'text'},  # ADD HERE ANALYZER
                    'feature_params': {
                        'type': 'nested',
                        'properties': {
                            'name': {'type': 'keyword'},
                            'required': {'type': 'boolean'},
                            'default': {'type': 'keyword'}
                        }
                    },
                    'feature_name': {'type': 'keyword'}}}}
    return mapp


def get_history_mapping():
    """Defines mapping for Jarvis conversation history index"""
    mapp = {
        'settings': {
            'number_of_shards': 1,
            'number_of_replicas': 0},
        'mappings': {
            'properties': {  # noqa
                'conversation_id': {'type': 'keyword'},
                'speaker': {'type': 'keyword'},
                'posted_at': {'type': 'date'},
                'content': {'type': 'text'}
            }
        }
    }
    return mapp
