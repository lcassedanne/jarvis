import sys
from elasticsearch import helpers


def inject_data(cnx, data, index):
    """Inject data into an Elasticsearch index.

    :param cnx: Elasticsearch connection.
    :type cnx: elasticsearch.Elasticsearch
    :param data: Data to inject into the index.
    :type data: list
    :param index: Name of the Elasticsearch index.
    :type index: str

    """
    helpers.bulk(cnx, data, index=index)
    cnx.indices.flush(index)


def create_index(cnx, mapping, index, force=False):
    """Creates and set mapping for a given index"""
    if force:
        confirm = True
    else:
        msg = 'Do you want to reset {} index ? [yes/No]: '
        confirm = input(msg.format(index))
        confirm = True if confirm.lower().startswith('y') else False
    if not confirm:
        sys.exit()
    else:
        if cnx.indices.exists(index=index):
            delete_index(cnx, index)
        cnx.indices.create(index=index, body=mapping)


def delete_index(cnx, index):
    """ delete an index"""
    try:
        cnx.indices.delete(index=index, ignore=400)
    except Exception:
        print('{} not found!'.format(index))
