from jarvis.config import (
    cnx,
    ES_KNOWLEDGE_INDEX
)


class KnowledgeRetrieval:
    def __init__(self):
        pass

    def run(self, params):
        info_title = params.get('info_title')
        if not info_title:
            raise ValueError('"info_title" parameter is required')
        body = {
            "query": {
                "match": {
                    "info_title": info_title
                }
            }
        }
        res = cnx.search(index=ES_KNOWLEDGE_INDEX, body=body, size=10)
        res = res['hits']['hits']
        if not res:
            return {"code": 404,
                    "request": ("Jarvis indique qu'il n'a pas trouvé "
                                "d'information à ce sujet dans la base "
                                "de données de sa mémoire.")}
        result = res[0]['_source']
        info_value = result.get('info_value')
        if not info_value:
            raise ValueError('Could not find a value for this information')

        return {"code": 200, "request": ("La valeur de l'information est "
                                         f"\"{info_value}\"")}
