from datetime import datetime, timezone

from jarvis.config import (
    cnx,
    ES_KNOWLEDGE_INDEX
)


class KnowledgeStorage:
    def __init__(self):
        pass

    def run(self, params):
        info_title = params.get('info_title')
        info_value = params.get('info_value')
        if not info_title:
            raise ValueError('"info_title" parameter is required')
        if not info_value:
            raise ValueError('"info_value" parameter is required')
        doc = {"info_title": info_title,
               "info_value": info_value,
               "stored_at": datetime.now(tz=timezone.utc)}
        cnx.index(ES_KNOWLEDGE_INDEX, doc)
        return {"code": 200, "request": ("Jarvis confirme qu'il a stocké "
                                         "l'information suivante: "
                                         f"{info_title}: {info_value}")}
