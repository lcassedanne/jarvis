import os
from datetime import datetime, timedelta
from google.auth.transport.requests import Request
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
import pickle

from jarvis.data import DATA_SOURCE
from jarvis.config import GOOGLE_OAUTH2_CREDENTIALS
from jarvis.vocals.output import JarvisOutput


class Calendar:
    def __init__(self):
        self.token_folder = os.path.join(DATA_SOURCE, 'google_tokens')
        self.audio_output_path = "/tmp/jarvis_output.mp3"
        self.JO = JarvisOutput(self.audio_output_path)
        self._check_credentials()

    def _check_credentials(self):
        credentials = None
        token_path = os.path.join(self.token_folder, 'calendar_token.pickle')
        if os.path.exists(token_path):
            with open(token_path, 'rb') as token:
                credentials = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not credentials or not credentials.valid:
            if (credentials
                and credentials.expired
                and credentials.refresh_token):
                credentials.refresh(Request())
            else:
                self.JO.formulate(content=("J'ai besoin de votre autorisation "
                                           "pour accéder à votre calendrier."),
                                  vocal=True,
                                  lg="fr-FR")
                flow = InstalledAppFlow.from_client_secrets_file(
                    GOOGLE_OAUTH2_CREDENTIALS,
                    scopes=['https://www.googleapis.com/auth/calendar'])
                credentials = flow.run_local_server(port=0)
            # Save the credentials for the next run
            with open(token_path, 'wb') as token:
                pickle.dump(credentials, token)

        self.service = build('calendar', 'v3', credentials=credentials)

    def run(self, params):
        start_time = params['start_time']
        end_time = params.get('end_time')
        if not end_time:
            print('No end_time specified: Default to 1 hour')
            start_time_dt = datetime.fromisoformat(start_time)
            end_time_dt = start_time_dt + timedelta(hours=1)
            end_time = end_time_dt.strftime("%Y-%m-%dT%H:%M:%S")
        event = {
            'summary': params['title'],
            'start': {
                'dateTime': start_time,
                'timeZone': 'Europe/Paris',
            },
            'end': {
                'dateTime': end_time,
                'timeZone': 'Europe/Paris',
            },
        }
        event = self.service.events().insert(calendarId='primary',
                                             body=event).execute()
        message = ("Jarvis confirme qu'un événement a été créé "
                   f"avec succès pour le {event['start']['dateTime']} "
                   f"avec le titre \"{event['summary']}\"")
        return {"code": 200,
                "request": message}


#     def run(self, params):
#         # Create an event
#         if 'action' in params and params['action'].lower() == 'create':
#             event = {
#                 'summary': params['summary'],
#                 'start': {
#                     'dateTime': params['start_time'],
#                     'timeZone': 'Europe/Paris',
#                 },
#                 'end': {
#                     'dateTime': params['end_time'],
#                     'timeZone': 'Europe/Paris',
#                 },
#             }
#             event = self.service.events().insert(calendarId='primary',
#                                                  body=event).execute()
#             message = ("Jarvis confirme qu'un événement a été créé "
#                        f"avec succès pour le {event['start']['dateTime']} "
#                        f"avec le titre \"{event['summary']}\"")
#             return {"code": 200,
#                     "request": message}
# 
#         # List events
#         elif 'action' in params and params['action'].lower() == 'list':
#             # Call the Calendar API - 'Z' indicates UTC time
#             now = datetime.datetime.utcnow().isoformat() + 'Z'
#             print('Getting the upcoming 10 events')
#             events_result = self.service.events().list(
#                 calendarId='primary', timeMin=now,
#                 maxResults=10, singleEvents=True,
#                 orderBy='startTime'
#             ).execute()
#             events = events_result.get('items', [])
# 
#             if not events:
#                 message = ("Jarvis ne trouve pas d'événement dans votre "
#                            "calendrier. Essayez de rafraîchir la page.")
#                 return {"code": 200,
#                         "request": message}
# 
#             message = ("Jarvis fournit une liste des 10 prochains "
#                        "événements:\n\"\"\"")
#             for event in events:
#                 start = event['start'].get(
#                     'dateTime', event['start'].get('date'))
#                 message += (f"\n- Titre: {event['summary']}\n"
#                             f"- Début: {start}")
#             message += "\"\"\""
#             return {"code": 200,
#                     "request": message}
# 
#         # Update an event
#         elif 'action' in params and params['action'].lower() == 'update':
#             event = self.service.events().get(
#                 calendarId='primary', eventId=params['event_id']).execute()
#             event['summary'] = params['summary']
#             updated_event = self.service.events().update(
#                 calendarId='primary',
#                 eventId=params['event_id'],
#                 body=event
#             ).execute()
# 
#             message = ("Jarvis confirme que l'événement a été mis à jour "
#                        "avec succès. Le titre est maintenant "
#                        f"\"{updated_event['summary']}\"")
#             return {"code": 200,
#                     "request": message}
# 
