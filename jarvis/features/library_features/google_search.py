import webbrowser


class GoogleSearch:
    def __init__(self):
        pass

    def run(self, params):
        base_url = "http://www.google.com/search?q="
        final_url = base_url + params['query'].replace(" ", "+")
        webbrowser.open_new(final_url)
        message = ("Jarvis confirme qu'il affiche actuellement les résultats "
                   f"pour \"{params['query']}\" à l'écran")
        return {"code": 200, "request": message}
