import openai
import requests

from jarvis.config import (
    NEWS_API_KEY,
    OPENAI_API_KEY
)


openai.api_key = OPENAI_API_KEY


class NewsFeed:
    def __init__(self):
        pass

    def run(self, params):
        url = "https://newsapi.org/v2/top-headlines"
        request_params = {
            "pageSize": 10,
            "apiKey": NEWS_API_KEY,
            "sources": "google-news-fr,le-monde,les-echos,liberation",
            # "country": "fr",
        }
        if params.get('query') != "all":
            request_params['q'] = params['query']
        news_response = requests.get(url=url, params=request_params)
        news_dict = news_response.json()
        if news_response.status_code != 200:
            message = ("Jarvis indique qu'il a rencontré une erreur en "
                       "utilisant l'API NewsFeed et essaye de donner une "
                       "explication. Voici l'erreur:\n"
                       "\"\"\"\n"
                       f"{news_dict['message']}\n"
                       "\"\"\"")
            return {"code": "500",
                    "request": message}
        news_string = ""
        for i, article in enumerate(news_dict['articles']):
            news_string += (f"\n{i+1}. - Titre: {article['title']}\n"
                            f"- Description: {article['description']}")
        message = ("Jarvis résume ces gros titres de journaux "
                   "en quelques phrases:\n"
                   f"\"\"\"{news_string}\"\"\"")
        return {"code": 200, "request": message}
