from jarvis.config import OPENAI_API_KEY
from jarvis.gpt.utils import build_conversation_string

import copy
import openai
from datetime import datetime

openai.api_key = OPENAI_API_KEY


def get_context(language="fr-FR"):
    """ """
    # Add base context at the beginning
    base_context = get_base_context(language)
    # Add useful contextual informations
    date_str = datetime.now().strftime("%c")
    # Put everything together
    context = (
        f"{base_context}\n"
        f"Date et heure: {date_str}\n"
        "Les personnes que Jarvis connait sont Louis, Marion (sa copine), et Pierre (son grand frère), et personne d'autre.\n"
    )
    return context


def get_base_context(language):
    if language == "fr-FR":
        base_context = (
            "Tu t'appelles Jarvis, et tu es "
            "un protocole d'assistance virtuelle créé "
            "par Louis Cassedanne sur son temps libre. Il t'a appelé "
            "Jarvis car il est un grand fan des films Iron Man. "
            "Tu fonctionnes grâce au modèle GPT-3, auquel Louis a ajouté "
            "plusieurs fonctionnalités pour te donner la capacité de prendre "
            "des actions. "
            "Dans cette conversation, tu parles avec Louis. "
            "Louis est né le 25 Juillet 1995, et il a commencé à te coder "
            "en Décembre 2022."
        )
    elif language == "en-US":
        base_context = (
            "Jarvis is a virtual assistance protocol designed and "
            "created by Louis Cassedanne on his free time. He named it "
            "Jarvis because he is a huge fan of the Iron Man movies."
        )
    return base_context


def get_context_old(conversation, language="fr-FR", depth=1000):
    """
    Based on last exchanges, query gpt model to get conversation context
    in one sentence.

    :param list exchanges: Last exchanges
    :param str language: language currently being used

    :returns: A sentence to describe the conversation context
    :rtype: str
    """
    # Cut exchanges to avoid too big query for Curie model
    full_exchanges = copy.deepcopy(conversation)
    full_exchanges.reverse()
    current_depth = 0
    exchanges = []
    for exchange in full_exchanges:
        ex_content = exchange.get("content", "")
        ex_depth = len(ex_content.split(" "))
        if current_depth + ex_depth < depth:
            exchanges.append(exchange)
        else:
            break
    exchanges.reverse()

    # Check exchanges format
    if not exchanges:
        if language == "fr-FR":
            conv_context = "C'est le début de leur conversation."
        elif language == "en-US":
            conv_context = "It is the beginning of their conversation."
        else:
            raise ValueError(f'Language "{language}" is not supported')
    else:
        # Ask gpt for context
        if language == "fr-FR":
            request = "Résume la conversation suivante en une phrase"
            output_marker = "Résumé:"
        elif language == "en-US":
            request = "Sum up the following conversation in one sentence"
            output_marker = "Summary:"
        else:
            raise ValueError(f'Language "{language}" is not supported')
        conversation = build_conversation_string(exchanges)
        # Create main prompt
        main_prompt = f"{request}\n\n{conversation}\n{output_marker}"
        # Get Curie model response
        response = openai.Completion.create(
            model="text-curie-001", prompt=main_prompt, temperature=0, max_tokens=500
        )
        # Remove new line at the beginning
        conv_context = response.choices[0].text.strip()
    # Add base context at the beginning
    base_context = get_base_context(language)
    context = f"{base_context} {conv_context}"
    return context
