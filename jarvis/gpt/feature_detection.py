from jarvis.config import OPENAI_API_KEY
from jarvis.gpt.utils import build_conversation_string

import openai

openai.api_key = OPENAI_API_KEY


def davinci_select_feature(command, feature_candidates,
                           exchanges, context, language):
    """
    Main method to detect if a feature needs to be run.

    :param dict feature_candidates: All features that could be requested.
    :param str command: The user last command

    :returns: Jarvis' answer
    :rtype: str
    """
    if not feature_candidates:
        raise ValueError('Variable "feature_candidates" cannot be empty '
                         'or null')
    # Create actions string
    actions_string = ""
    for i in feature_candidates:
        actions_string += f'{i}. {feature_candidates[i]["feature_context"]}\n'
    # Build previous conversation string
    conversation = build_conversation_string(exchanges)
    # Create main prompt
    context_headline = "Contexte de la conversation"
    following_headline = "Suite de la conversation:"
    question_headline = ("Dans la dernière réplique de l'utilisateur, "
                         "laquelle de ces actions a été demandée?:")
    answer_headline = ("Réponse (mettre uniquement le numéro de la réponse "
                       "et rien d'autre):")
    main_prompt = (f'{context_headline}: {context}\n\n'
                   f'{following_headline}\n'
                   f'{conversation}\n'
                   f'{question_headline}\n'
                   f'{actions_string}\n\n'
                   f'{answer_headline}')
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=main_prompt,
        temperature=0,
        max_tokens=1000
    )
    answer_nb = response.choices[0].text.strip().replace('.', '')
    try:
        answer_nb = int(answer_nb)
    except ValueError:
        try:
            answer_nb = int(answer_nb.split(' ')[0])
        except ValueError:
            raise ValueError('Could not deduct action to take '
                             f'(Davinci answer: {answer_nb}')
    output_with_meta = {
        "usage": response.usage,
        "main_prompt": main_prompt,
        "output": answer_nb
    }
    return output_with_meta


def davinci_select_feature_params(command, feature,
                                  exchanges, context,
                                  language):
    """
    Main method to detect if a feature needs to be run.

    :param dict feature: Feature to run
    :param str command: The user last command

    :returns: Jarvis' answer
    :rtype: str
    """
    if not feature.get('feature_params'):
        return {}
    # Create actions string
    questions_string = "0. Quel est le film préféré de Louis?\n"
    for i, param in enumerate(feature.get('feature_params', [])):
        questions_string += f'{i+1}. {param["question"]}\n'
    # Build previous conversation string
    conversation = build_conversation_string(exchanges)
    # Create main prompt
    context_headline = "Contexte de la conversation"
    following_headline = "Suite de la conversation:"
    question_headline = ("Réponds à ces questions (si l'information n'est "
                         "pas donnée, répondre \"non spécifié\"):")
    answer_headline = "Réponses:"

    main_prompt = (f'{context_headline}: {context}\n\n'
                   f'{following_headline}\n'
                   f'{conversation}\n'
                   f'{question_headline}\n'
                   f'{questions_string}\n\n'
                   f'{answer_headline}\n'
                   '0. Iron Man\n'
                   '1.')
    response = openai.Completion.create(
        model="text-davinci-003",
        prompt=main_prompt,
        temperature=0,
        max_tokens=1000
    )
    answers = response.choices[0].text.strip()
    # Parse answers
    # params_answers = [el[3:] for el in answers.split('\n')]
    params = feature.get('feature_params', [])
    for i, answer in enumerate(answers.split('\n')):
        if i == 0:  # No need to remove first 3 characters
            params[i]['value'] = answer.replace('\"', '')
        else:
            params[i]['value'] = answer[3:].replace('\"', '')

    output_with_meta = {
        "usage": response.usage,
        "main_prompt": main_prompt,
        "output": params
    }
    return output_with_meta
