from jarvis.config import OPENAI_API_KEY
from jarvis.gpt.utils import build_conversation_string

import openai

openai.api_key = OPENAI_API_KEY


def prompt_davinci(context, exchanges, language, clipboard=None):
    """
    Main method to get Jarvis' response.

    :param str prompt: The user's line
    :param str context: A sentence to describe the context of the conversation
    until now
    :param str language: Language currently being used
    :param str clipboard: To add if we want to pass clipboard content to jarvis

    :returns: Jarvis' answer
    :rtype: str
    """
    if not exchanges:
        raise ValueError('Variable "exchanges" cannot be empty ' "or null")
    # Build previous conversation string
    conversation = build_conversation_string(exchanges)
    # Create clipboard string
    clipboard_string = ""
    if clipboard:
        clipboard_string = '"""\n' f"{clipboard}\n" '"""\n'
    # Create main prompt
    request = "Contexte de la conversation"
    main_prompt = (
        f"{request}: {context}\n\n"
        f"{conversation}"
        f"{clipboard_string}"
        'Réponse de Jarvis: "'
    )
    new_prompt = [{"role": "system", "content": context}]
    for exchange in exchanges:
        if exchange["speaker"] == "Jarvis":
            new_prompt.append({"role": "assistant", "content": exchange["content"]})
        else:
            new_prompt.append({"role": "user", "content": exchange["content"]})
    response = openai.ChatCompletion.create(
        model="gpt-3.5-turbo",
        messages=new_prompt,
        temperature=0.7,
    )
    output = response.choices[0]["message"]["content"]
    output_with_meta = {
        "usage": response.usage,
        "main_prompt": main_prompt,
        "output": output,
    }
    return output_with_meta


def prompt_davinci_after_feature(feature_output, exchanges, context, language):
    """
    Main method to get Jarvis' response after a command has been run.

    :param dict feature_output: a dictionary describing the state of the
    command after having been run
    :param str prompt: The user's line
    :param str context: A sentence to describe the context of the conversation
    until now

    :returns: Jarvis' answer
    :rtype: str
    """
    if not exchanges:
        raise ValueError('Variable "exchanges" cannot be empty ' "or null")
    if language != "fr-FR":
        raise ValueError(f'Language "{language}" is not supported')
    # Build previous conversation string
    conversation = build_conversation_string(exchanges)
    # Create main prompt
    request = "Contexte de la conversation"
    main_prompt = (
        f"{request}: {context}\n\n"
        f"{conversation}"
        f'- Jarvis ({feature_output.get("request")}):'
    )
    response = openai.Completion.create(
        model="text-davinci-003", prompt=main_prompt, temperature=0.7, max_tokens=1000
    )
    # Remove new line at the beginning
    output = response.choices[0].text.strip()
    output_with_meta = {
        "usage": response.usage,
        "main_prompt": main_prompt,
        "output": output,
    }
    return output_with_meta
