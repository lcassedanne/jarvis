from jarvis.config import OPENAI_API_KEY

import openai


openai.api_key = OPENAI_API_KEY


def build_conversation_string(exchanges):
    """ """
    from pprint import pprint

    pprint(exchanges)
    conversation = ""
    for element in exchanges:
        speaker = element.get("speaker", "")
        content = element.get("content", "")
        new_line = f'- {speaker}: "{content}"\n'
        conversation += new_line
    return conversation
