from pprint import pprint
from time import sleep
import pyperclip
import threading
from datetime import datetime, timedelta, timezone
import traceback
import uuid

import jarvis.features as jfeatures
from jarvis.config import cnx, ES_HISTORY_INDEX, ES_FEATURES_INDEX
from jarvis.vocals.output import JarvisOutput
from jarvis.vocals.input import JarvisInput
from jarvis.gpt.main_dialog import prompt_davinci, prompt_davinci_after_feature
from jarvis.gpt.feature_detection import (
    davinci_select_feature,
    davinci_select_feature_params,
)
from jarvis.gpt.context import get_context


class Jarvis:
    def __init__(self):
        self.conversation_id = str(uuid.uuid4())
        self.jarvis_vocal = True
        self.language = "fr-FR"
        self.audio_output_path = "/tmp/jarvis_output.mp3"
        self.audio_path = "/tmp/jarvis_input.wav"
        self.vocal_button = "Key.cmd_r"
        self.write_button = "Key.alt"
        self.tokens_count = 0
        self._active = True
        self.conversation = []
        self.context = ""
        self.threads = []
        self.clipboard_content = pyperclip.paste()
        self.clipboard_last_update = datetime.now() - timedelta(seconds=60)

        # Modules to talk and listen to Jarvis
        self.JI = JarvisInput(self.audio_path, self.vocal_button, self.write_button)
        self.JO = JarvisOutput(self.audio_output_path)

        self.last_output_select_feature = {}
        self.last_output_select_feature_params = {}
        self.last_output_davinci_after_feature = {}
        self.last_output_davinci = {}

    @property
    def active(self):
        return self._active

    @property
    def last_exchanges(self):
        return self.conversation[-10:]

    def shutdown(self):
        self.JO.formulate(
            content="A la prochaine, monsieur.",
            vocal=self.jarvis_vocal,
            lg=self.language,
        )
        self._active = False

    def _search_feature(self, command):
        # body = {"query": {"match": {"feature_context": command}}}
        body = {"query": {"match_all": {}}}
        res = cnx.search(index=ES_FEATURES_INDEX, body=body, size=10000)
        res = res["hits"]["hits"]
        return [doc["_source"] for doc in res]

    def _analyze_command(self, command):
        """
        Main function to analyze a string command and decide
        which action to run as an answer
        """
        res = self._search_feature(command)
        if not res:  # No candidates, avoid an unnecessary request to davinci
            return
        features = {i + 1: doc for i, doc in enumerate(res)}
        features[len(res) + 1] = {"feature_context": "Aucune"}
        output = davinci_select_feature(
            command, features, self.conversation[-1:], self.context, self.language
        )
        self.last_output_select_feature = output
        selected_feature = output["output"]
        if not selected_feature:
            return

        feature = features[selected_feature]
        if feature["feature_context"] == "Aucune":
            return

        print(f'[SELECTED FEATURE] : {feature["feature_context"]}')
        output = davinci_select_feature_params(
            command, feature, self.last_exchanges, self.context, self.language
        )
        self.last_output_select_feature_params = output
        feature_params = output.get("output", [])
        print(f"[FEATURE PARAMS] : {feature_params}")
        # Format params for run method and stop if missing required
        formatted_params = {}
        for param in feature_params:
            if param.get("required") and param.get("value") == "Non spécifié":
                print(f'[MISSING PARAM] - {param["name"]}')
                return
            if param.get("required"):
                formatted_params[param.get("name")] = param.get("value")
            if (not param.get("required")) and (param.get("value") == "Non spécifié"):
                formatted_params[param.get("name")] = param.get("default")
            else:
                formatted_params[param.get("name")] = param.get("value")
        module = getattr(jfeatures, feature["feature_name"])
        try:
            feature_output = module().run(params=formatted_params)
        except Exception:
            feature_output = {
                "code": "500",
                "request": (
                    "Jarvis indique qu'il a rencontré une erreur et "
                    "ne peut donc pas accéder à la requête de "
                    "l'utilisateur. Il essaye de trouver une "
                    "explication. Voici l'erreur: \n"
                    '"""\n'
                    f"{traceback.format_exc()}\n"
                    '"""'
                ),
            }
            print("\n\n")
            print(">" * 15, "\n")
            print("[ERROR] [FEATURE CODE] " f"[{feature.get('feature_name')}]")
            traceback.print_exc()
            print("\n")
            print(">" * 15, "\n\n")
        return feature_output

    def _get_user_command(self):
        jarvis_input = JarvisInput()
        command = jarvis_input.get_input()
        return command

    def _log_record(self, content, who="jarvis"):
        # Add to conversations list
        if who == "user":
            speaker = "Louis"
        else:
            speaker = "Jarvis"
        record = {
            "speaker": speaker,
            "content": content,
            "conversation_id": self.conversation_id,
            "posted_at": datetime.now(tz=timezone.utc),
        }
        self.conversation.append(record)
        # Inject in ES
        cnx.index(ES_HISTORY_INDEX, record)

    def run(self):
        # Launch clipboard listener
        t = threading.Thread(target=self._launch_clipboard_listener)
        self.threads.append(t)
        t.start()
        # Launch main conversation
        t = threading.Thread(target=self._launch_conversation)
        self.threads.append(t)
        t.start()

    def _launch_clipboard_listener(self):
        while self._active:
            old_content = self.clipboard_content
            self.clipboard_content = pyperclip.paste()
            if self.clipboard_content != old_content:
                self.clipboard_last_update = datetime.now()
            sleep(1)

    def _launch_conversation(self):
        while self._active:
            # Update conversation context
            try:
                self.context = get_context()
                command = self._get_user_command()
                if command == "exit":
                    self.shutdown()
                elif command == "show":
                    pprint(self.conversation)
                elif command == "context":
                    pprint(self.context)
                elif command == "cb":
                    pprint(self.clipboard_content)
                    print("Last update:", self.clipboard_last_update)

                elif command == "prompt":
                    print("\n=== DAVINCI AFTER FEATURE")
                    print(self.last_output_davinci_after_feature.get("main_prompt"))
                    print(self.last_output_davinci_after_feature.get("output"))
                    print("\n=== SELECT FEATURE")
                    print(self.last_output_select_feature.get("main_prompt"))
                    print(self.last_output_select_feature.get("output"))
                    print("\n=== SELECT FEATURE PARAMS")
                    print(self.last_output_select_feature_params.get("main_prompt"))
                    print(self.last_output_select_feature_params.get("output"))
                    print("\n=== DAVINCI MAIN")
                    print(self.last_output_davinci.get("main_prompt"))
                    print(self.last_output_davinci.get("output"))
                elif command == "vocal":
                    if self.jarvis_vocal:
                        print("deactivating jarvis vocal")
                        self.jarvis_vocal = False
                    else:
                        print("activating jarvis vocal")
                        self.jarvis_vocal = True
                elif command == "tokens":
                    print("Total tokens used:", self.tokens_count)
                    print("Estimated cost:", f"${self.tokens_count * 0.002 / 1000}")
                else:
                    self._log_record(command, "user")
                    # feature_output = self._analyze_command(command)
                    # if feature_output:
                    #     output = \
                    #         prompt_davinci_after_feature(feature_output,
                    #                                      self.last_exchanges,
                    #                                      self.context,
                    #                                      self.language)
                    #     main_answer = output['output']
                    #     self.last_output_davinci_after_feature = output
                    # else:
                    #     clip_content = None
                    #     clip_diff = datetime.now() - self.clipboard_last_update
                    #     if clip_diff.seconds <= 10:
                    #         clip_content = self.clipboard_content
                    #         print('<CLIPBOARD ADDED TO REQUEST>')
                    #     output = prompt_davinci(self.context,
                    #                             self.last_exchanges,
                    #                             self.language,
                    #                             clipboard=clip_content)
                    #     main_answer = output['output']
                    #     self.last_output_davinci = output
                    output = prompt_davinci(
                        self.context,
                        self.last_exchanges,
                        self.language,
                        clipboard=None,
                    )
                    main_answer = output["output"]
                    self.last_output_davinci = output
                    self.JO.formulate(
                        content=main_answer, vocal=self.jarvis_vocal, lg=self.language
                    )
                    self._log_record(main_answer, "Jarvis")
                    self.tokens_count += output.get("usage", {}).get("total_tokens", 0)
            except Exception:
                message = (
                    "J'ai rencontré une erreur qui provient de mon "
                    "code source. La voici à l'écran."
                )
                self.JO.formulate(
                    content=message, vocal=self.jarvis_vocal, lg=self.language
                )
                print("\n\n")
                print(">" * 15, "\n")
                print("[ERROR] [SOURCE CODE]")
                traceback.print_exc()
                print("\n")
                print(">" * 15, "\n\n")
