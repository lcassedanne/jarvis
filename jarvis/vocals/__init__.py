from pkg_resources import get_distribution
from pkg_resources import DistributionNotFound
from pkg_resources import resource_filename

PACKAGE_SOURCE = resource_filename(__name__, '/')

try:
    __version__ = get_distribution(__name__).version
except DistributionNotFound:
    # package is not installed
    pass
