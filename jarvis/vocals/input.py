from datetime import datetime
from pynput import keyboard

from jarvis.vocals.speech_to_text.recorder import (
    recorder,
    player
)
from jarvis.vocals.speech_to_text.converter import (
    convert_audio_to_text
)


class JarvisInput:
    def __init__(self,
                 audio_path="/tmp/jarvis_input.wav",
                 vocal_button="Key.cmd_r",
                 write_button="Key.alt"):
        self.input_method = "vocal"
        self.audio_path = audio_path
        self.vocal_button = vocal_button
        self.recorder = recorder(audio_path)
        self.player = player(audio_path)

    def on_press(self, key):
        if key is None:  # unknown event
            pass
        elif isinstance(key, keyboard.Key):  # special key event
            if str(key) == self.vocal_button and self.player.playing == 0:
                self.recorder.start()
        elif isinstance(key, keyboard.KeyCode):  # alphanumeric key event
            pass

    def on_release(self, key):
        if str(key) == "Key.alt":
            self.recorder.stop()
            self.input_method = "written"
            return False
        elif isinstance(key, keyboard.Key):  # special key event
            if str(key) == self.vocal_button:
                self.recorder.stop()
                return False
        elif isinstance(key, keyboard.KeyCode):  # alphanumeric key event
            pass

    def get_input(self):
        now = datetime.now().strftime("%H:%M:%S")
        print("\n{} ".format(now) + '-' * 20)
        command = None
        while not command:
            with keyboard.Listener(on_press=self.on_press,
                                   on_release=self.on_release) as listener:
                listener.join()
            if self.input_method == "vocal":
                try:
                    command = convert_audio_to_text(self.audio_path)
                except IndexError:
                    # no audio to convert, try again
                    print('No audio to convert, try again:')
                    continue
                print('[SPOKEN COMMAND]', command)
            else:
                command = input('[WRITTEN COMMAND] ')
            print("-" * 29)
        return command
