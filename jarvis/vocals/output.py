from jarvis.vocals.text_to_speech.google import text_to_speech


class JarvisOutput:
    def __init__(self, audio_path="/tmp/jarvis_output.mp3"):
        self.audio_path = audio_path

    def formulate(self, content, vocal, lg="fr-FR"):
        """
        This is jarvis method to say things.
        """
        print("\n" + content + "\n")
        if vocal:
            try:
                text_to_speech(
                    text=self.remove_code(content), audio_path=self.audio_path
                )
            except Exception as e:
                print("[ERROR] Could not run text to speech method")
                print(e)

    def remove_code(self, content):
        code_count = content.count("```")
        for i in range(code_count):
            content = content.split("```")[0] + "".join(content.split("```")[1:])
        return content
