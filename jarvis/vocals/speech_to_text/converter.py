from google.cloud import speech
import os
import io

import openai
from jarvis.config import OPENAI_API_KEY

openai.api_key = OPENAI_API_KEY


def convert_audio_to_text(audio_path):
    transcript = ""
    with open(audio_path, "rb") as audio_file:
        transcript = openai.Audio.transcribe("whisper-1", audio_file)
        transcript = transcript.to_dict().get("text")
    return transcript 


# GOOGLE VERSION
from jarvis.config import GOOGLE_APPLICATION_CREDENTIALS

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = GOOGLE_APPLICATION_CREDENTIALS
client = speech.SpeechClient()

# The path of your audio file
file_name = "OSR_us_000_0010_8k.wav"


def convert_audio_to_text_old(audio_path):
    with io.open(audio_path, "rb") as audio_file:
        content = audio_file.read()
        audio = speech.RecognitionAudio(content=content)

    speech_context = {
        "phrases": [
            "thread",
            "jarvis",
            "l'API",
            "API",
            "python",
            "neo4j",
            "elasticsearch",
        ],
        "boost": 20
    }

    config = speech.RecognitionConfig(
        encoding=speech.RecognitionConfig.AudioEncoding.LINEAR16,
        enable_automatic_punctuation=True,
        audio_channel_count=1,
        language_code="fr-FR",
        speech_contexts=[speech_context],
        model="command_and_search",
        enable_word_confidence=True
    )

    # Sends the request to google to transcribe the audio
    response = client.recognize(request={"config": config, "audio": audio})
    # Reads the response
    return response.results[0].alternatives[0].transcript
    # for result in response.results:
    #     print("Transcript: {}".format(result.alternatives[0].transcript))
