from threading import Lock
from pynput import keyboard
import pyaudio
import wave


class listener(keyboard.Listener):
    def __init__(self, recorder, player):
        super().__init__(on_press=self.on_press, on_release=self.on_release)
        self.recorder = recorder
        self.player = player

    def on_press(self, key):
        if key is None:  # unknown event
            pass
        elif isinstance(key, keyboard.Key):  # special key event
            if str(key) == "Key.ctrl" and self.player.playing == 0:
                self.recorder.start()
        elif isinstance(key, keyboard.KeyCode):  # alphanumeric key event
            if key.char == 'q':  # press q to quit
                if self.recorder.recording:
                    self.recorder.stop()
                return False  # this is how you stop the listener thread
            if key.char == 'p' and not self.recorder.recording:
                self.player.run()

    def on_release(self, key):
        print("RELEASED:", )
        if key is None:  # unknown event
            pass
        elif isinstance(key, keyboard.Key):  # special key event
            if str(key) == "Key.ctrl":
                self.recorder.stop()
                return False
        elif isinstance(key, keyboard.KeyCode):  # alphanumeric key event
            if str(key) == "s":
                self.recorder.stop()
                return
            pass


class player:
    def __init__(self, wavfile):
        self.wavfile = wavfile
        self.playing = 0  # don't try to record while the wav file is in use
        self.lock = Lock()  # incrementing and decrementing self.playing safely

    # contents of the run function are processed in another thread
    # so we use the blocking
    # version of pyaudio play file example:
    # http://people.csail.mit.edu/hubert/pyaudio/#play-wave-example
    def run(self):
        with self.lock:
            self.playing += 1
        with wave.open(self.wavfile, 'rb') as wf:
            p = pyaudio.PyAudio()
            stream = p.open(format=p.get_format_from_width(wf.getsampwidth()),
                            channels=wf.getnchannels(),
                            rate=wf.getframerate(),
                            output=True)
            data = wf.readframes(8192)
            while data != b'':
                stream.write(data)
                data = wf.readframes(8192)

            stream.stop_stream()
            stream.close()
            p.terminate()
            wf.close()
        with self.lock:
            self.playing -= 1


class recorder:
    def __init__(self,
                 wavfile,
                 chunksize=8192,
                 dataformat=pyaudio.paInt16,
                 channels=1,
                 rate=16000):
        self.filename = wavfile
        self.chunksize = chunksize
        self.dataformat = dataformat
        self.channels = channels
        self.rate = rate
        self.recording = False
        self.pa = pyaudio.PyAudio()

    def start(self):
        # we call start and stop from the keyboard listener, so we use the
        # asynchronous version of pyaudio streaming. The keyboard listener
        #  must regain control to begin listening again for the key release.
        if not self.recording:
            self.wf = wave.open(self.filename, 'wb')
            self.wf.setnchannels(self.channels)
            self.wf.setsampwidth(self.pa.get_sample_size(self.dataformat))
            self.wf.setframerate(self.rate)

            def callback(in_data, frame_count, time_info, status):
                # file write should be able to keep up with audio data stream
                # (about 1378 Kbps)
                self.wf.writeframes(in_data)
                return (in_data, pyaudio.paContinue)

            self.stream = self.pa.open(format=self.dataformat,
                                       channels=self.channels,
                                       rate=self.rate,
                                       input=True,
                                       stream_callback=callback)
            self.stream.start_stream()
            self.recording = True
            print('recording started')

    def stop(self):
        if self.recording:
            self.stream.stop_stream()
            self.stream.close()
            self.wf.close()

            self.recording = False
            print('recording finished')
