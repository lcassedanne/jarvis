#
# With AWS
#


from jarvis.config import (
    AWS_REGION_NAME,
    AWS_ACCESS_KEY_ID,
    AWS_SECRET_ACCESS_KEY
)

import boto3
from botocore.exceptions import BotoCoreError, ClientError
from contextlib import closing
import os
import sys
from playsound import playsound


client = boto3.client(
    'polly',
    region_name=AWS_REGION_NAME,
    aws_access_key_id=AWS_ACCESS_KEY_ID,
    aws_secret_access_key=AWS_SECRET_ACCESS_KEY
)


def text_to_speech(content, lg):
    try:
        response = client.synthesize_speech(
            Engine='standard',
            LanguageCode=lg,
            OutputFormat='mp3',
            Text=content,
            VoiceId='Celine'
        )
    except (BotoCoreError, ClientError) as e:
        # The service returned an error, exit gracefully
        print(f'AWS error: {e}')
        sys.exit(-1)

    # Access the audio stream from the response
    if "AudioStream" in response:
        # Note: Closing the stream is important because the service throttles
        # on the number of parallel connections. Here we are using
        # contextlib.closing to ensure the close method of the stream object
        # will be called automatically at the end of the with statement's
        # scope.
        with closing(response["AudioStream"]) as stream:
            output = os.path.join("/tmp", "jarvis_last_speech.mp3")

            try:
                # Open a file for writing the output as a binary stream
                with open(output, "wb") as file:
                    file.write(stream.read())
            except IOError as error:
                # Could not write to file, exit gracefully
                print(error)
                sys.exit(-1)

    else:
        # The response didn't contain audio data, exit gracefully
        print("Could not stream audio")
        sys.exit(-1)

    try:
        playsound(output)
    except Exception as e:
        print(f'Playsound error: {e}')
        sys.exit(-1)
