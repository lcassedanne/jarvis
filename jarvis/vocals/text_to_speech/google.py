#
# With Google
#

import os
from google.cloud import texttospeech
from playsound import playsound

from jarvis.config import GOOGLE_APPLICATION_CREDENTIALS

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = GOOGLE_APPLICATION_CREDENTIALS
client = texttospeech.TextToSpeechClient()


def text_to_speech(text, audio_path):
    # Convert text to mp3 file
    synthesis_input = texttospeech.SynthesisInput(text=text)

    voice = texttospeech.VoiceSelectionParams(
        language_code="fr-FR", name="fr-FR-Standard-D"
    )

    audio_config = texttospeech.AudioConfig(
        pitch=-3,
        speaking_rate=1.3,
        audio_encoding=texttospeech.AudioEncoding.MP3
    )

    response = client.synthesize_speech(
        input=synthesis_input, voice=voice, audio_config=audio_config
    )

    with open(audio_path, 'wb') as out:
        out.write(response.audio_content)
    # Read mp3 file
    playsound(audio_path)
