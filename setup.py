from setuptools import setup, find_packages

requirements = """
boto3==1.26.28
coverage
openai
google-cloud-texttospeech
google-cloud-speech==2.16.2
pynput==1.7.6
pyperclip==1.8.2
elasticsearch==7.17.0
pandas==1.5.2
pyaudio==0.2.12
newsapi-python==0.2.6
"""

setup(
    name='jarvis',
    setup_requires=['setuptools_scm'],
    # use_scm_version = {'write_to': '../version.txt', 'root': '..'},
    author='Cassedanne Technology',
    author_email='louiscassedanne@gmail.com',
    install_requires=requirements,
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    scripts=['scripts/call_jarvis'],
)
